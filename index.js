var fs = require('fs')

exports.TowerBlockFs = function (config) {
    this.get = function (id) {

        return fs.readFile(id)
            .then(function (err,data) {
                if (err){
                    console.error(err)
                    return err
                }
                if (config.object) {
                    return JSON.parse(data)
                }
                return data.toString()
            })
    }

    this.set = function (id, data) {
        var dataToSave = data

        if (typeof dataToSave != "string") {
            dataToSave = JSON.stringify(data)
        }

        return fs.writeFile(id,dataToSave)
            .then(function (err,res) {
                if (err){
                    console.error(err)
                    return err
                }
                console.log("File Saved");
                return res
            })
    }
}
